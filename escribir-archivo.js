const fs = require('fs');

module.exports =(path,mensaje)=>{
    return new Promise((resolve,reject)=>{
        fs.writeFile(path, mensaje,{},(errorAlGuardar)=>{
            if(errorAlGuardar){
                reject({
                    mensaje: 'Error al crear fichero',
                    error: errorAlGuardar
                })
            }else{
                resolve({
                    mensaje: 'Se creo archivo correctamente',
                })
            }

        });
    })
}