const suma= require('./sumar.js'); //Cualquiera de las dos maneras 
const resta= require('./restar.js');
const multiplica= require('./multiplicar.js');

module.exports = {
    suma,
    resta,
    multiplica
}