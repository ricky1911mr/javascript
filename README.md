## Uso JavaScript 

A continuaciòn se detallarà ejemplos de todo lo que se puede realizar en JavaScript

a={
    nombre: 'Ricardo',
    apellido:'Paredes',
    cedula:1725905440,
    direccion: 'Villaflora',
    estadoCivil: 'Soltero',
    sexo: 'Masculino',
    edad: '25 AÑOS',
    universidad: 'Escuela Politécnica Nacional',
    fechaNacimiento: new Date('2000-10-25'),
    cel: 0984901383,
    tipoSangre: 'O+',
        familia:{
            padre:{
                nombre: 'Marlon',
                apellido:'Paredes',
                cedula:1706572755,
                direccion: 'Patronato San Jose',
                estadoCivil: 'Soltero',
                sexo: 'Masculino',
                edad: '58 AÑOS',
                universidad: 'NO',
                fechaNacimiento: new Date('1960-06-12'),
                cel: 093073916,
                tipoSangre: 'O+',
            },
        }
    }
    console.log(a)


    var b=5
    var c=6
    console.log(b*c)


* No se necesita ";" al terminar cada lìnea de comando
* JavaScript en las operaciones fundamentales se encarga de ver que es lo mas conveniente hacer segùn el tipo de variable

Operador	Operación	    Sintaxis
+	        Adición	        var total = sumando1 + sumando2;
                            // asigna a total la suma 
                            // de las otras dos variables
-	        Sustracción	    var total = minuendo - sustraendo;
                            // asigna a total la resta
                            // de las otras dos variables
*	        Multiplicación	var total = factor1 * factor2;
                            // asigna a total el producto 
                            // de las otras dos variables
/	        División	    var total = dividendo / divisor;
                            // asigna a total el cociente 
                            // de las otras dos variables
%	        Módulo de división var total = dividendo % divisor;
                            // asigna a total el resto 
                            // de la división de las dos variables