module.exports.buscar = (arregloUsuarios,usuarioABuscar)=>{
    const usuarioEncontrado = arregloUsuarios.find((usuario)=>{
        return usuarioABuscar === usuario
    })

    return new Promise((resolve,reject)=>{
        if(usuarioEncontrado){  
            resolve({
            mensaje: 'Usuario encontrado',
            usuarioEncontrado: usuarioEncontrado
            })
        }else{
            reject({
            mensaje: 'Usuario no encontrado',
            usuarioEncontrado: usuarioEncontrado
            })
        }
    }) //Revisar bien promesas 
} 
