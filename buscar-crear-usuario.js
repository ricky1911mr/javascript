 module.exports.crearBuscarUsuario = (arregloUsuarios,usuarioABuscarOCrear)=>{
        buscar(arregloUsuarios,usuarioABuscarOCrear, (resultadoBusqueda)=>{
            if(resultadoBusqueda.usuarioEncontrado){
                return new Promise((resolve)=>{  
                        resolve({
                        mensaje: 'Usuario encontrado',
                        usuario: resultadoBusqueda.usuarioEncontrado
                        })
                    })
            }else{
        crear(arregloUsuarios,usuarioABuscarOCrear,(resultadoUsuarioCreado)=>{
            return new Promise((reject)=>{
                reject({
                    mensaje: 'Usuario no encontrado',
                    arregloUsuarios: resultadoUsuarioCreado.arregloUsuarios
                })
            })
        })
            }                    
        })
    })
