


// buscar
module.exports.buscarUsuario2 = (arregloUsuarios,usuarioABuscar,cb)=>{
    const usuarioEncontrado = arregloUsuarios.find((usuario)=>{
        return usuarioABuscar === usuario
    })

    if(usuarioEncontrado){
        cb({
            mensaje:'usuario encontrado',
            usuarioEncontrado: usuarioEncontrado
        })
    }
    else{
        cb({
            mensaje:'usuario no encontrado',
            usuarioEncontrado: usuarioEncontrado
        })
    }
    
} 

// buscar o crear


module.exports = (arregloUsuarios,usuarioABuscarOCrear,cb)=>{
    buscarUsuario2(arregloUsuarios,usuarioABuscarOCrear, (resultadoBusqueda)=>{
        if(resultadoBusqueda.usuarioEncontrado){
            cb({
                mensaje: 'usuario encontrado',
                usuario: resultadoBusqueda.usuarioEncontrado
            })
        }else{
            crear2(arregloUsuarios,usuarioABuscarOCrear,(resultadoUsuarioCreado)=>{
                cb({
                    mensaje: 'no se encontro y se creo',
                    arregloUsuarios: resultadoUsuarioCreado.arregloUsuarios
                })
            })
        }
    })
} 

//Crear
module.exports.crear2 = (arregloUsuarios,usuarioACrear,cb)=>{
    arregloUsuarios.push(usuarioACrear)
    cb({
        mensaje:'Se creo',
        arregloUsuarios : arregloUsuarios
    })
    }