const leerArchivodesdePc = require('./leer-archivo1.js');
const escribirArchivo = require('./escribir-archivo.js');

leerArchivodesdePc('texto1.txt')
.then((resultadoPromesa)=>{
    console.log('Resultado promesa', resultadoPromesa)
    return escribirArchivo('otroArchivo.txt','Contenido nuevo')
    
})
.then(resultadoPromesaCrear=>{
    console.log('Resultado promesa crear', resultadoPromesaCrear)
    return leerArchivodesdePc('otroArchivo.txt')
})
.then((resultadoPromesaCreadoLeido)=>{
    console.log('Resultado promesa creado leido', resultadoPromesaCreadoLeido)
})
.catch((errorPromesa)=>{
    console.log(errorPromesa)
})
