var arregloNumeros = [1, 2, 3, 4, 5, 6, 7]

var conEvery = arregloNumeros.every(valor => {
    return typeof valor === 'number'
})
console.log('some', conEvery)

var conSome = arregloNumeros.some(valor => {
    return typeof valor === 'string'
})
console.log(conSome)

var conSome = arregloNumeros.some(valor => {
    return typeof valor === 'boolean'
})
console.log(conSome)

var suma = 0
arregloNumeros.forEach(valor => {
    suma += valor
})
console.log(suma)

var valorInicialAcumulador = 12
var totalConReduce = arregloNumeros.reduce((acumulador, valor) => {
    //console.log('Acumulador', acumulador)
    return acumulador + valor
}, valorInicialAcumulador);
console.log('Valor total con Reduce', totalConReduce)


var propietarioJason = [
    { nombre: 'Ricardo', /* edadPersona: 18, */ sexo: 'Masculino', mascota: 'Dosty'/* , edadMascota:6 */ },
    { nombre: 'Felipe', /* edadPersona: 22,  */sexo: 'Masculino', mascota: 'Firulais'/* , edadMascota:4 */},
    { nombre: 'Diana', /* edadPersona: 26, */ sexo: 'Femenino', mascota: 'Cachetes'/* ,edadMascota:8 */},
]

var sumaEdades =function(persona,mascota){
    return (persona+mascota)*3
}

var propietarioMasculino = propietarioJason.reduce((acumulador, valor) => {
    var esMasculino = valor.sexo === 'Masculino'
    if (esMasculino) {
        acumulador.push(valor)
        acumulador.push(sumaEdades(valor.edadPersona,valor.edadMascota))
    }
    return acumulador
}, []/*Si en vez de [] pongo propietarioJason se aumentaria al final*/)
//console.log('Propietarios Masculinos', typeof propietarioMasculino)
//console.log('Propietarios Masculinos', propietarioMasculino)


var propietarioFemenino = propietarioJason.reduce((acumulador, valor) => {
    var esFemenino = valor.sexo === 'Femenino'
    if (esFemenino) {
        acumulador.push(valor)
        acumulador.push(sumaEdades(valor.edadPersona,valor.edadMascota))
    }
    return acumulador
}, [])
//console.log('Propietarios Masculinos', typeof propietarioMasculino)
//console.log('Propietarios Femeninos', propietarioFemenino)
//console.log('Propietarios Masculino original', propietarioJason)

var nuevoArregloPropietario = propietarioJason.map((valor)=>{
    valor.edadPropietario = Math.floor(Math.random()*100)
    valor.edadMascota = Math.floor(Math.random()*10)
    valor.totalEdades = sumaEdades(valor.edadPropietario,valor.edadMascota)
    return valor
}).reduce((acumulador,valor)=>{
    var esMasculino = valor.sexo === 'Masculino'
    if (esMasculino) {
        acumulador.push(valor)
    }
    return acumulador
},[]).filter((valor)=>{
    if(valor.edadMascota%2!=0)
    return valor
})
console.log(nuevoArregloPropietario)