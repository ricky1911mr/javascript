const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');


const numeros$ = of(1,2,3,'2',4,5,6,7)
const respuesta =(respuesta)=>{
    console.log('respuesta', respuesta)
}

const errorObservable =(error)=>{
    console.log('error',error)
}

const cuandoFinaliza=()=>{
    console.log('Ya finaliza todo')
}

function sumarUno(numero){
   // console.log('Esta sumando')
    return Number(numero)+1
}
function multiplicarPorDos(numero){
    //console.log('Esta multiplicando')
    return numero*2
}
function filtrarPares(numero){
    //console.log('Esta filtrando pares')
    return numero%2===0
}

function promesaNumeroPar(numero){
    return new Promise((resolve, reject)=>{
        if(numero%2===0){
            resolve(numero)
        }else{
            reject(numero)
        }
    })
}

const ejecutarPromesa =(numero)=>{
 //archivo={
     //nombreFichero: '${numero}.txt'

 }
 
    const promesaObservable$ = from(promesaNumeroPar(numero))  // from(crearArchivo(archivo))
    return promesaObservable$
}

numeros$
.pipe(

    map(sumarUno),
    mergeMap(ejecutarPromesa)
    /* map(multiplicarPorDos),
    filter(filtrarPares) */
   


)
.subscribe(respuesta, errorObservable, cuandoFinaliza)
 

//Crear ficheros con el nombre de cada numero

/* const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');


const numeros$ = of(1, 1, 3, 4,'3', 6, 7)
const respuesta = (respuesta) => {
    console.log('respuesta', respuesta)
}
const errorObservable = (error) => {
    console.log('error', error)
}
const cuandoFinaliza = () => {
    console.log('ya finalizo todo')
}

function sumarUno(numero) {
    // console.log('esta sumando')
    //  throw ({error:'error'})
    return Number(numero) + 1
}
function multiplicarPorDos(numero) {
    // console.log('esta multiplicando')
    // throw ({error:'error'})
    return numero * 2
}
function filtrarPares(numero){
    return numero % 2 === 0
}


function promesaNumeroPar(numero){

   console.log('numero en promesa', numero)
return new Promise((resolve, reject)=>{

    if(numero % 2 === 0 ){
        resolve(numero)
    }else{
        reject(numero)
    }
 

   
})
}


const ejecutarPromesa =(numero)=>{
    archivo= {
        nombreFichero: `${numero}.txt`,
        contenido:`cualquier cosa mas ${numero}`
    }
    
    const promesaObservable$ = from(crearArchivo(archivo))
    return promesaObservable$
}

numeros$
    .pipe(
        map(sumarUno),
        mergeMap(ejecutarPromesa)    
    )
    .subscribe(respuesta, errorObservable, cuandoFinaliza)  */