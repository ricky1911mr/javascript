const fs = require('fs');

module.exports =(path)=>{
    return new Promise((resolve,reject)=>{
        fs.readFile(path,'utf8',(errorAlLeer,contenido)=>{
            if(errorAlLeer){
                reject({
                    mensaje: 'Hubo un error al leer',
                    error: errorAlLeer
                })
            }else{
                resolve({
                    mensaje: 'Se leyo correctamente',
                    error: errorAlLeer, 
                    contenido,

                })
            }

        });
    })

}